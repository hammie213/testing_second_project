package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Driver;

import java.util.List;

public class UnitedAirlinesBasePage {

    public UnitedAirlinesBasePage(){
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(css = ".app-components-GlobalHeader-globalHeader__expandedTabHeader--1Ra2H")
    public List<WebElement> menuOptions;

    @FindBy(id = "travelTab")
    public WebElement travelTab;

    @FindBy(id = "statusTab")
    public WebElement statusTab;

    @FindBy(id = "checkInTab")
    public WebElement checkInTab;

    @FindBy(id = "tripsTab")
    public WebElement tripsTab;

    @FindBy(id = "roundtrip")
    public WebElement roundTripSelection;

    @FindBy(id = "oneway")
    public WebElement oneWaySelection;

    @FindBy(css = "input[id='award']")
    public WebElement bookWithMiles;

    @FindBy(id = "flexibleDates")
    public WebElement flexibleDates;

    @FindBy(id = "bookFlightOriginInput")
    public WebElement originInput;

    @FindBy(id = "bookFlightDestinationInput")
    public WebElement destinationInput;

    @FindBy(id = "DepartDate")
    public WebElement departDate;

//    @FindBy(id = "ReturnDate")
//    public WebElement returnDate;


//    @FindBy(css = "button[aria-labelledby='stuff']")
    @FindBy(id = "passengerMenuId")
    public WebElement travelersDropdown;

    @FindBy(css = "button[aria-label=\"Substract one Adult\"]")
    public WebElement addAdultButton;

    @FindBy(id = "cabinType")
    public WebElement selectCabinType;

    @FindBy(css = "button[aria-label='Find flights']")
    public WebElement searchButton;

    @FindBy(id = "cabinType_item-2")
    public WebElement businessOrFirst;
}
