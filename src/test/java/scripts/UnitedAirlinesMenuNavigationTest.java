package scripts;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.UnitedAirlinesBasePage;
import utils.Waiter;

public class UnitedAirlinesMenuNavigationTest extends UnitedAirlinesBase{

    @BeforeMethod
    public void setPage(){
        unitedAirlinesBasePage = new UnitedAirlinesBasePage();
    }

    @Test(priority = 1, description = "Validating Main Menu Navigation Items")
    public void validatingMainMenu(){

        String[] menuOptionsTexts = {"BOOK",
                "MY TRIPS",
                "TRAVEL INFO",
                "MILEAGEPLUS® PROGRAM",
                "DEALS",
                "HELP"};

        for(int i = 0; i < menuOptionsTexts.length; i++){
            Assert.assertTrue(unitedAirlinesBasePage.menuOptions.get(i).isDisplayed());
            Assert.assertEquals(unitedAirlinesBasePage.menuOptions.get(i).getText(), menuOptionsTexts[i]);
        }
    }

    @Test(priority = 2, description = "Validate Book Travel Menu")
    public void validatingBookTravelMenu(){

        String[] bookMenuTexts = {"Book", "Flight status",
                "Check-in",
                "My trips"};

        Assert.assertTrue(unitedAirlinesBasePage.travelTab.isDisplayed());
        Assert.assertEquals(unitedAirlinesBasePage.travelTab.getText(), bookMenuTexts[0]);

        Assert.assertTrue(unitedAirlinesBasePage.statusTab.isDisplayed());
        Assert.assertEquals(unitedAirlinesBasePage.statusTab.getText(), bookMenuTexts[1]);

        Assert.assertTrue(unitedAirlinesBasePage.checkInTab.isDisplayed());
        Assert.assertEquals(unitedAirlinesBasePage.checkInTab.getText(), bookMenuTexts[2]);

        Assert.assertTrue(unitedAirlinesBasePage.tripsTab.isDisplayed());
        Assert.assertEquals(unitedAirlinesBasePage.tripsTab.getText(), bookMenuTexts[3]);
    }

    @Test(priority = 3, description = "Validating radio buttons")
    public void validatingRadioButtons(){

//        Assert.assertTrue(unitedAirlinesBasePage.roundTripSelection.isDisplayed());
        Assert.assertTrue(unitedAirlinesBasePage.roundTripSelection.isEnabled());
        Assert.assertTrue(unitedAirlinesBasePage.roundTripSelection.isSelected());
//        Assert.assertTrue(unitedAirlinesBasePage.oneWaySelection.isDisplayed());
        Assert.assertTrue(unitedAirlinesBasePage.oneWaySelection.isEnabled());
        Assert.assertFalse(unitedAirlinesBasePage.oneWaySelection.isSelected());

        unitedAirlinesBasePage.oneWaySelection.click();
        Assert.assertFalse(unitedAirlinesBasePage.roundTripSelection.isSelected());
        Assert.assertTrue(unitedAirlinesBasePage.oneWaySelection.isSelected());

    }

    @Test(priority = 4, description = "Validating Book with miles and Flexible dates")
    public void validatingCheckboxes(){

//        Assert.assertTrue(unitedAirlinesBasePage.bookWithMiles.isDisplayed());
        Assert.assertTrue(unitedAirlinesBasePage.bookWithMiles.isEnabled());
        Assert.assertFalse(unitedAirlinesBasePage.bookWithMiles.isSelected());

//        Assert.assertTrue(unitedAirlinesBasePage.flexibleDates.isDisplayed());
        Assert.assertTrue(unitedAirlinesBasePage.flexibleDates.isEnabled());
        Assert.assertFalse(unitedAirlinesBasePage.flexibleDates.isSelected());

        unitedAirlinesBasePage.bookWithMiles.click();
        unitedAirlinesBasePage.flexibleDates.click();

        Assert.assertTrue(unitedAirlinesBasePage.bookWithMiles.isSelected());
        Assert.assertTrue(unitedAirlinesBasePage.flexibleDates.isSelected());

        unitedAirlinesBasePage.bookWithMiles.click();
        unitedAirlinesBasePage.flexibleDates.click();

        Assert.assertFalse(unitedAirlinesBasePage.bookWithMiles.isSelected());
        Assert.assertFalse(unitedAirlinesBasePage.flexibleDates.isSelected());


    }

    @Test(priority = 5, description = "Validating one way ticket from ORD to MIA")
    public void validatingTicket(){
        unitedAirlinesBasePage.oneWaySelection.click();

        unitedAirlinesBasePage.originInput.sendKeys("Chicago, IL, US (ORD)");
        unitedAirlinesBasePage.destinationInput.sendKeys("Miami, FL, US (MIA)");

        unitedAirlinesBasePage.departDate.clear();
        unitedAirlinesBasePage.departDate.sendKeys("Feb 28");

        unitedAirlinesBasePage.travelersDropdown.click();
        unitedAirlinesBasePage.addAdultButton.click();

        unitedAirlinesBasePage.selectCabinType.click();
        unitedAirlinesBasePage.businessOrFirst.click();

        unitedAirlinesBasePage.searchButton.click();
    }

}
